<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 */
get_header(); ?>

<div class="well">
	<?php //get_search_form(); ?>
	<h1>404 error - Sorry, that page does not exist</h1>
</div>>
<div class="row">
	<div class="span4">
		<h2>All Pages</h2>
		<?php wp_page_menu(); ?>
	</div>
	<div class="span4">
		<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
	</div>
</div>
<?php get_footer(); ?>