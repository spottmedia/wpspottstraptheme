<?php
	/*
	Template Name: Blank
	Provided for the "favourites" page initially
	*/

	get_header();

	echo apply_filters('the_content',wpautop(trim($post->post_content)));

	get_footer();
?>