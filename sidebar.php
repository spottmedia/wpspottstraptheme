<?php
/**
 * The Sidebar containing the main widget areas.
 *
 */
?>

<div class="span4" id="sidebar">
	<?php
		/*
		 * <div class="faux_banner faux_MPU"></div><div class="faux_banner faux_MPU"></div>
		 */
		if ( function_exists('dynamic_sidebar')) dynamic_sidebar("sidebar");
	?>
</div>
