<?php

	get_header();
	echo '<div class="span12 append-bottom" id="search_result_content">';
	if (have_posts()) :

		// set up disaply parameters
		$params = array();
		$params['thumbnail_size'] = 'search-thumbnail';
		$params['li_class'] = 'span4';
		$params['hide_text'] = true;
		$params['hide_social'] = true;
		$params['hide_tags'] = true;
		$params['social_tag_in_popover'] = true;

		echo '<h1>'.sprintf( __( 'Search results for \'%s\'', 'spottstraptheme' ), $s).'</h1>';;

		echo '<ul class="standard_loop">';
		$counter = 1;
		while ( have_posts() ) : the_post();

			$post = $wp_query->post;
			$isFavourited = wpfp_check_favorited($post->ID);
			$params['favourite_add'] = !$isFavourited;
			$params['favourite_remove'] = $isFavourited;
			
			echo spottstraptheme_post_loop($post,$params,$count);
			if($counter % 3 == 0) {
				// nicer grids for when images aren't unified
				echo '<span class="span12"></span>';
			}
			$counter++;
		endwhile;
		echo '</ul>';

		spott_content_nav('nav-below');

	else :
		echo '<h2>'.sprintf(__('No posts found. Try a different search?','spottstraptheme'),$s).'</h2>';
		echo get_search_form();
	endif;

	echo '</div>';
	get_footer();
?>
