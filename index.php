<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 */

get_header();

global $wp_query;

$heading = '';

if(is_tax()){
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$heading = $term->name;
}else if(is_category()){
	$heading = single_cat_title( '', false );
}else if(is_tag()){
	$heading = single_tag_title( '', false );
}

//spott_big_dump($wp_query->posts);
?>
<div class="span8">
<?php
	echo '<h1>'.$heading.'</h1>';
	if (have_posts()) :
		echo '<ul class="standard_loop divider">';
		$count = 0;
		while ( have_posts() ) : the_post();
			$count++;
            $params = array();
			$thispost=$wp_query->post;
			echo spottstraptheme_post_loop($thispost,$params,$count);
		endwhile;
		echo '</ul>';
	endif;
    //comments_template();
    if ( function_exists('spott_content_nav') ){
        spott_content_nav('nav-below');
    }
?>
</div>
<?php get_sidebar('sidebar'); ?>
<?php get_footer(); ?>
