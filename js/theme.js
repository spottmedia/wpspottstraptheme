jQuery(document).ready(function ($) {
    //Trigger the sizing function as soon as anything happens - http://stackoverflow.com/questions/11188964/fluid-input-append-in-bootstrap-responsive
    $(document).ready(sizing);
    $(window).resize(sizing);
	$.cookieCuttr({
		cookieNotificationLocationBottom: true,
		cookieAnalytics: false,
		cookiePolicyLink: '/privacy',
		cookieAcceptButtonText: 'OK',
		cookieMessage: 'We use cookies to help make our site better, your use of this site confirms your acceptance. <a href="{{cookiePolicyLink}}" title="read about our cookies">Read about our cookies here</a>'
	});
	//prevent console.log errors
    try {
        console.assert(1);
    } catch (e) {
        console = { log:function () {
        }, assert:function () {
        } };
    }
    //Simple "exists() function"
    $.fn.exists = function () {
        return $(this).length > 0
    };

    //Neat sizing trick to keep form fields full width
    function sizing() {
        var formfilterswidth = $("#s").parents('.span4:first').width();
        //alert(formfilterswidth);
        $("#s").width((formfilterswidth - 40) + "px");
    };

});
