jQuery(document).ready(function($) {
	Galleria.loadTheme("/wp-content/themes/spottstraptheme/js/vendor/galleria/themes/classic/galleria.classic.js");
	$("#standard_gallery").galleria({
			showInfo: false,
	        extend: function() {
		        this.play(4000); // will advance every 4th second
		    }
	    });

})