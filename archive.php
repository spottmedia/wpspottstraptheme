<?php
/**
 * The template for displaying all posts.
 *
 * Default Post Template
 *
 */

get_header();

global $wp_query;

$heading = '';

if(is_tax()){
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$heading = $term->name;
}elseif(is_category()){
	$heading = single_cat_title( '', false );
}elseif(is_tag()){
	$heading = single_tag_title( '', false );
}elseif ( is_day() ){
	$heading = sprintf( __( 'Daily Archives: %s', 'twentyfourteen' ), get_the_date() );
}elseif ( is_month() ){
	$heading = sprintf( __( 'Monthly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );
}elseif ( is_year() ){
	$heading = sprintf( __( 'Yearly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );
}else{
	$heading = __( 'Archives', 'twentyfourteen' );
}




//spott_big_dump($wp_query->posts);
?>
<div class="span8">
<?php
	echo '<h1>'.$heading.'</h1>';
	echo category_description();
	if (have_posts()) :
		echo '<ul class="standard_loop divider">';
		$count = 0;
		while ( have_posts() ) : the_post();
			$count++;
            $params = array();
			$params['string_count'] = 40;
			$params['thumbnail_size'] = 'category-loop-thumbnail';
			$params['hide_social'] = true;
			$params['hide_tags'] = true;
			$thispost=$wp_query->post;
			echo spottstraptheme_post_loop($thispost,$params,$count);
		endwhile;
		echo '</ul>';
	endif;
    //comments_template();
    if ( function_exists('spott_content_nav') ){
        spott_content_nav('nav-below');
    }
?>
</div>
<?php get_sidebar('sidebar'); ?>
<?php get_footer(); ?>
