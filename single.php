<?php
/**
 * The template for displaying single posts
 *
 * Template Name: Default Single Post
 * Description: Page template with a content container and right sidebar
 *
 */

get_header(); ?>
	<div class="span8">
	<?php
		echo '<h1>'.$post->post_title.'</h1>';
		if(defined('ADDTHIS_INIT')){
			do_action('addthis_widget',get_permalink($post->ID), get_the_title($post->ID), 'fb_tw_p1_sc');
		}else{
		echo function_exists('spott_socialbuttons')?'<div class="social">'.spott_socialbuttons(get_permalink()).'</div>':'';
		}
		echo '<div class="hr2 prepend-top"><hr /></div>';
		echo apply_filters('the_content',wpautop(trim($post->post_content)));
	?>
	<footer class="entry-meta prepend-top-3">
		<?php
			echo '<div class="hr2 prepend-top"><hr /></div>';
			$author_id=$post->post_author;
			//$authorstuff = get_user_meta($author_id);
			//spott_big_dump($authorstuff);
			if ( get_the_author_meta( 'description', $author_id ) ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries. ?>
			<div class="author-info imagecolwrap">
				<div class="author-avatar imagecol">
					<?php
						echo get_avatar( get_the_author_meta( 'user_email', $author_id ), 68 );
					?>
				</div><!-- .author-avatar -->
				<div class="author-description textcol">
					<h2><?php printf( __( 'About %s', 'spottstrap' ), get_the_author_meta( 'display_name', $author_id ) ); ?></h2>
					<p><?php the_author_meta( 'description', $author_id ); ?></p>
					<div class="author-link">
						<?php
							if ( defined( 'WPSEO_VERSION' ) ) {
								$include_rel_author = false;
							}else{
								$include_rel_author = true;
							}

							/*
							 * 					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>" <?php echo ($include_rel_author)?'rel="author"':''; ?>>
							<?php echo sprintf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'spottstrap' ), get_the_author_meta( 'display_name', $author_id ) ); ?>
						</a>
							 */
						?>

						<?php
							if($googleplus = get_the_author_meta( 'googleplus', $author_id )){
								echo '<p>'.sprintf( __( 'Follow %s on <a href="%s" target="_blank">Google+</a>', 'spottstrap' ), get_the_author_meta( 'first_name', $author_id ),$googleplus ).'</p>';
							}
							if($twitter = get_the_author_meta( 'twitter', $author_id )){
								echo '<p>'.sprintf( __( 'Follow %s on <a href="https://twitter.com/%s" target="_blank">Twitter</a>', 'spottstrap' ), get_the_author_meta( 'first_name', $author_id ),$twitter ).'</p>';
							}
							if($facebook = get_the_author_meta( 'facebook', $author_id )){
								echo '<p>'.sprintf( __( 'Friend %s on <a href="https://www.facebook.com/%s" target="_blank">Facebook</a>', 'spottstrap' ), get_the_author_meta( 'first_name', $author_id ),$facebook ).'</p>';
							}
							if($website = get_the_author_meta( 'user_url', $author_id )){
								echo '<p>'.sprintf( __( 'Checkout %s\'s <a href="%s" target="_blank">website</a>', 'spottstrap' ), get_the_author_meta( 'first_name', $author_id ),$website ).'</p>';
							}
						?>

					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php
			//Try to include the jetpack subscription form if it's provided by default
			if ( shortcode_exists( 'mc4wp_form' ) ) {
				echo '<div class="hr2 prepend-top"><hr /></div>';
				echo '<h3>'.__('Sign up for our newsletter','spottstrap').'</h3>';
				echo do_shortcode( '[mc4wp_form]' );
			}

			echo '<div class="hr2 prepend-top"><hr /></div>';
			comments_template( '', true ); ?>
	</footer>
	</div>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
