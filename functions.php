<?php
	define('DYN_SCRIPT_VERSION', uniqid ());

	/** Run spottstraptheme_setup() when the 'after_setup_theme' hook is run. */
	add_action('after_setup_theme', 'spottstraptheme_setup');
	if (!function_exists('spottstraptheme_setup')):

		function spottstraptheme_setup() {

			// This theme styles the visual editor with editor-style.css to match the theme style.
			add_editor_style();

			// This theme uses post thumbnails
            add_theme_support('post-thumbnails');
			add_image_size( 'post-thumbnail', 240, 160);
			add_image_size( 'standard-loop-thumbnail', 160, 160);
			add_image_size( 'category-loop-thumbnail', 80, 80);

        // Add default posts and comments RSS feed links to head
        add_theme_support('automatic-feed-links');

        // Make theme available for translation
        $themelocaleloaded = load_theme_textdomain('spottstraptheme', TEMPLATEPATH . '/languages');

			// This theme uses wp_nav_menu() in one location.
			register_nav_menus( array(	 'SuperNav' => __('Super Navigation'),
										 'TopNav' => __('Top Navigation'),
										 'FooterNav' => __('Footer Navigation'),
			) );


			/*
			 * Register all of the custom content components - post types, taxonomies and resultant paths
			 */
			add_action('init', 'register_custom_content_components');
			function register_custom_content_components() {

				flush_rewrite_rules();
			}

		}
	endif;

/*
	 * Widget Support
	 */
	if ( function_exists('register_sidebar') ){
		register_sidebar(array('name'=> 'Sidebar'));
	}


	/*
	 * Model functions query overrides
	 */


	/*
	 *
	 * View/Layout helpers
	 *
	 */

	//Enqueue scripts correctly
	function spottstraptheme_enqueue_scripts() {
		if (!is_admin()) {
			wp_enqueue_script('jquery');
			wp_enqueue_style('spottstraptheme_site_css', get_template_directory_uri().'/css/site.css', false ,DYN_SCRIPT_VERSION, 'all' );
			wp_enqueue_script('spottstraptheme_modernizr_js', get_template_directory_uri().'/js/modernizr.js', null,DYN_SCRIPT_VERSION, true );
			wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/js/bootstrap.js', array('jquery'),DYN_SCRIPT_VERSION, true );
			wp_enqueue_script('jquery.cookie', get_template_directory_uri().'/js/jquery.cookie.js', array('jquery'),DYN_SCRIPT_VERSION, true );
			wp_enqueue_script('cookiecuttr', get_template_directory_uri().'/js/vendor/cookiecuttr/jquery.cookiecuttr.js', array('jquery','jquery.cookie'),DYN_SCRIPT_VERSION, true );
			wp_enqueue_style('cookiecuttr', get_template_directory_uri().'/js/vendor/cookiecuttr/cookiecuttr.css', false ,DYN_SCRIPT_VERSION, 'all' );

			wp_enqueue_script('spottstraptheme_theme_js', get_template_directory_uri().'/js/theme.js', array('jquery'),DYN_SCRIPT_VERSION, true );
		}
	}
	add_action('wp_enqueue_scripts', 'spottstraptheme_enqueue_scripts');


	/*
	 * Augment standard gallery function with applicable galleria wrapper
	 */
	function spottstraptheme_get_gallery($post_id,$type,$id="standard_gallery",$class="standard_gallery",$image_size='large',$randomise=false){
		$returnVar = false;
		$gallery = spott_get_gallery($post_id,$type,$id="standard_gallery",$class="standard_gallery",$image_size='large',$randomise=false);

		if($gallery !== false){
			if($gallery['count'] > 1){
				wp_enqueue_script( 'spottstraptheme_galleria', get_template_directory_uri() . '/js/vendor/galleria/galleria-1.2.8.js',array('jquery'), DYN_SCRIPT_VERSION);
				wp_enqueue_style( 'spottstraptheme_galleria_default', get_template_directory_uri() . '/js/vendor/galleria/themes/classic/galleria.classic.css', false, DYN_SCRIPT_VERSION );
				wp_enqueue_script( 'spottstraptheme_galleria_default', get_template_directory_uri() . '/js/vendor/galleria/default.js',array('jquery','spottstraptheme_galleria'), DYN_SCRIPT_VERSION);
			}
			$returnVar = $gallery['gallery'];
		}
		return $returnVar;
	}




	/*
	 * Display Post Template
	 */
	function spottstraptheme_post_loop($thispost=null,$params=array(),$count=1){
		$string_count = isset($params['string_count'])?$params['string_count']:45;
		$heading_tag = isset($params['heading_tag'])?$params['heading_tag']:'h2';
		$hide_heading=isset($params['hide_heading'])?$params['hide_heading']:false;
        $hide_text=isset($params['hide_text'])?$params['hide_text']:false;
        $hide_meta=isset($params['hide_meta'])?$params['hide_meta']:true;
        $hide_social=isset($params['hide_social'])?$params['hide_social']:false;
        $hide_image=isset($params['hide_image'])?$params['hide_image']:false;
        $hide_tags=isset($params['hide_tags'])?$params['hide_tags']:false;
        $thumbnail_size=isset($params['thumbnail_size'])?$params['thumbnail_size']:'standard-loop-thumbnail';
        $li_class=isset($params['li_class'])?' class="'.$params['li_class'].'"':'';
        $before=isset($params['before'])?$params['before']:false;

        $returnVar = '';
        $permalink = get_permalink( $thispost->ID );
        $returnVar .= '<li'.$li_class.'>';
        $returnVar .= $before?$before:'';
		$returnVar .= '<div class="imagecolwrap">';
		$returnVar .= '<div class="imagecol">';
        if(! $hide_image){
            $returnVar .= '<a href="'.$permalink.'">';
            if ( has_post_thumbnail( $thispost->ID ) ) {
                $returnVar .= wp_get_attachment_image( get_post_thumbnail_id( $thispost->ID),$thumbnail_size,false,array('title' => trim(strip_tags( $thispost->post_title ))));
            }
            else {
                //REFERENCE:Get the dimensions of a thumbnail defined using add_image_size
                global $_wp_additional_image_sizes;
                $returnVar .= '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/thumbnail-default.png" height="'.$_wp_additional_image_sizes[$thumbnail_size]['height'].'" width="'.$_wp_additional_image_sizes[$thumbnail_size]['width'].'" />';
            }
        }

        // featch attached image data, to pass it further down to socialicons. Some of them might have a good use of it.
        $imageData = wp_get_attachment_image_src( get_post_thumbnail_id( $thispost->ID));

        $returnVar .= '</a>';
		$returnVar .= '</div>';
		$returnVar .= '<div class="textcol">';
        $returnVar .= $hide_heading?'':'<'.$heading_tag.' class="title"><a href="'.$permalink.'">'.$thispost->post_title.'</a></'.$heading_tag.'>';
       	//$returnVar .= ($hide_social || ! function_exists('spott_socialbuttons'))?'':'<div class="social">'.spott_socialbuttons($permalink, $imageData[0]).'</div>';
        $returnVar .= $hide_meta?'':'<p class="meta">'.spott_posted_on($thispost).'</p>';
        $returnVar .= $hide_text?'':'<p>'.wp_trim_words( preg_replace('/\[.*\]/', '',strip_shortcodes($thispost->post_content)),$string_count).'  <a href="'.$permalink.'">'.__('more &raquo;','spott').'</a></p>';//
        $returnVar .= ($hide_tags?'':'<p class="tag_list"><span class="title">Tags: </span>'.get_the_term_list($thispost->ID, array('post_tag'),'',', ','').'</p>');
		$returnVar .= '</div>';
		$returnVar .= '</div>';
        $returnVar .= '</li>';
        return $returnVar;
	}

	function spottstraptheme_posted_on($post) {
		$post_object = get_post_type_object($post->post_type);
		$returnVar = sprintf( __( '<i class="icons-clock iconblock"></i> <time class="entry-date" datetime="%1$s" pubdate>%2$s</time>', 'spottstraptheme' ),
			esc_attr( get_the_time() ),
			human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'
		);
		return $returnVar;
	}


/**
 * ====================================================
 * Help Contact Form 7 Play Nice With Twitter Bootstrap
 * ====================================================
 * Add a Twitter Bootstrap-friendly class to the "Contact Form 7" form
 */
add_filter( 'wpcf7_form_class_attr', 'wildli_custom_form_class_attr' );
function wildli_custom_form_class_attr( $class ) {
	$class .= ' form-horizontal';
	return $class;
}



	class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {


		function start_lvl( &$output, $depth ) {

			$indent = str_repeat( "\t", $depth );
			$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";

		}

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$li_attributes = '';
			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = ($args->has_children) ? 'dropdown' : '';
			$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
			$classes[] = 'menu-item-' . $item->ID;


			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = ' class="' . esc_attr( $class_names ) . '"';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

			if ( !$element )
				return;

			$id_field = $this->db_fields['id'];

			//display this element
			if ( is_array( $args[0] ) )
				$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
			else if ( is_object( $args[0] ) )
				$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'start_el'), $cb_args);

			$id = $element->$id_field;

			// descend only when the depth is right and there are childrens for this element
			if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

				foreach( $children_elements[ $id ] as $child ){

					if ( !isset($newlevel) ) {
						$newlevel = true;
						//start the child delimiter
						$cb_args = array_merge( array(&$output, $depth), $args);
						call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
				unset( $children_elements[ $id ] );
			}

			if ( isset($newlevel) && $newlevel ){
				//end the child delimiter
				$cb_args = array_merge( array(&$output, $depth), $args);
				call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
			}

			//end this element
			$cb_args = array_merge( array(&$output, $element, $depth), $args);
			call_user_func_array(array(&$this, 'end_el'), $cb_args);

		}

	}


	function spottstraptheme_comments(){
		return true;
	}

?>
