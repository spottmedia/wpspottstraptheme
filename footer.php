</div>
</div>
<footer>
    <div class="container" id="footer">
		<div class="wrapper">
			<div class="pull-left">
				<?php
					//, 'fallback_cb'=>false so it doesn't show if empty
					echo wp_nav_menu(array('theme_location' => 'FooterNav','container'=>false, 'menu_class' => 'menu inline', 'fallback_cb'=>false));
				?>
			</div>
			<div class="pull-right">
				<span>&copy; 2013 <?php bloginfo('name'); ?> <?php //the_time('Y') ?></span>
			</div>

			<?php
			if ( function_exists('dynamic_sidebar')) dynamic_sidebar("footer-content");
			?>
		</div>
    </div>
</footer>
<?php
if ( function_exists( 'yoast_analytics' ) ) {
	yoast_analytics();
}
?>
<?php wp_footer(); ?>
</body>
</html>
