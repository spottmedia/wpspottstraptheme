<?php
	get_header();
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	$author_id = $curauth->ID;
	global $wp_query;
	$heading = $curauth->user_firstname . ' ' . $curauth->user_lastname;
	//spott_big_dump($wp_query->posts);
?>
<div class="span8">
	<?php
		echo '<h1>'.$heading.'</h1>';

		echo '<div class="alignright">'.get_avatar( get_the_author_meta( 'user_email', $author_id ), 250 ).'</div>';

		echo $curauth->user_description;
		echo sprintf('<h2 class="prepend-top-2 clear">Posts by %s</h2>',$curauth->user_firstname);
		if (have_posts()) :
			echo '<ul class="standard_loop divider">';
			$count = 0;
			while ( have_posts() ) : the_post();
				$count++;
				$params = array();
				$params['string_count'] = 40;
				$params['thumbnail_size'] = 'category-loop-thumbnail';
				$params['hide_social'] = true;
				$params['hide_tags'] = true;
				$thispost=$wp_query->post;
				echo spottstraptheme_post_loop($thispost,$params,$count);
			endwhile;
			echo '</ul>';
		endif;
		//comments_template();
		if ( function_exists('spott_content_nav') ){
			spott_content_nav('nav-below');
		}
	?>
</div>
<?php get_sidebar('sidebar'); ?>
<?php get_footer(); ?>



<?php //spott_big_dump($curauth);
    //echo userphoto($author_id);
?>
