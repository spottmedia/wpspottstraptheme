<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php wp_title( '|', true, 'right' );?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri();?>/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri();?>/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri();?>/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri();?>/ico/apple-touch-icon-57-precomposed.png">
	<?php if(is_attachment()) : ?>
		<link rel="canonical" href="<?php echo get_permalink($post->post_parent) ?>" />
	<?php endif; ?>
    <!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<!--[if lt IE 7]>
	<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
	<![endif]-->
	<div id="fb-root"></div><!-- Required for FB shares -->
    <div class="container">
		<!--<div class="faux_LEADERBOARD prepend-top append-bottom"></div>-->
        <section id="header">
			<div class="row">
                <div class="span8" id="logo_wrapper">
                    <a href="/"><img src="<?php echo get_stylesheet_directory_uri().'/img/logo.png'; ?>" id="logo" /></a>
                </div>
                <div class="span4" id="supernav_menu_wrapper">
                    <?php
						//, 'fallback_cb'=>false so it doesn't show if empty
						echo wp_nav_menu(array('theme_location' => 'SuperNav','container'=>false, 'fallback_cb'=>false));
					?>
                </div>
                <div class="span8 dropdown" id="top_menu_wrapper">

					<div class="navbar">

						<div class="container">
							<a href="#" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
							<?php
								//, 'fallback_cb'=>false so it doesn't show if empty
								$args = array(
									'theme_location' => 'TopNav',
									'depth'		 => 2,
									'container' => 'div',
									'container_class'	 => 'nav-collapse collapse',
									'menu_class'	 => 'nav',
									'menu_id'	 => 'topnav',
									'walker'	 => new Bootstrap_Walker_Nav_Menu(),
									'fallback_cb'=>false
								);
								wp_nav_menu($args);
							?>
						</div>

					</div>

                </div>
                <div class="span4" id="search_wrapper">
					<div id="search_panel">
	                    <div id="search_body">
		                    <i class="icons-big_search iconblock"></i>
							<?php get_search_form(); ?>
						</div>
					</div>
                </div>
                <div class="span12 clear" id="clear_header_bottom"></div>
			</div>
		</section>
        <div class="row">