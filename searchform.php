<form role="search" class="form" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">

	<div class="input-append">
		<input class="input-medium" id="s" name="s" type="text" placeholder="Search this site">
        <span class="add-on" id="s-symbol"><i class="icon-search"></i></span>
	</div>

</form>
