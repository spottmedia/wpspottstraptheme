<?php
/**
 * The template for displaying all pages.
 *
 * Template Name: Default Page
 * Description: Page template with a content container and right sidebar
 *
 */

get_header(); ?>
	<div class="span8">
	<?php
		echo '<h1>'.$post->post_title.'</h1>';
		echo function_exists('spott_socialbuttons')?'<div class="social">'.spott_socialbuttons(get_permalink()).'</div>':'';
		echo '<div class="hr2 prepend-top"><hr /></div>';
		echo apply_filters('the_content',wpautop(trim($post->post_content)));
	?>
	</div>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
